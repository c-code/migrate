package migrate

import (
	"context"
	"errors"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"os"
	"testing"
	"time"
)

func TestMigrator_AddMigration_and_MigrateTo(t *testing.T) {
	type args struct {
		migration    Migrationer
		dependencies []string
	}
	tests := []struct {
		name          string
		tree          map[string]migrationNode
		args          args
		addingErr     error
		migratingErr  error
		expectedOrder []string
	}{
		{
			"single migration test",
			make(map[string]migrationNode),
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{},
			},
			nil,
			nil,
			[]string{"HEAD"},
		},
		{
			"migration on top of the chain",
			map[string]migrationNode{
				"A": {
					NewMigrationFunc("A", nil),
					[]string{},
				},
				"B": {
					NewMigrationFunc("B", nil),
					[]string{"A"},
				},
				"C": {
					NewMigrationFunc("C", nil),
					[]string{"B"},
				},
			},
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{"C"},
			},
			nil,
			nil,
			[]string{"A", "B", "C", "HEAD"},
		},
		{
			"migration on the middle of the chain",
			map[string]migrationNode{
				"A": {
					NewMigrationFunc("A", nil),
					[]string{},
				},
				"B": {
					NewMigrationFunc("B", nil),
					[]string{"A"},
				},
				"C": {
					NewMigrationFunc("C", nil),
					[]string{"B"},
				},
			},
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{"B"},
			},
			nil,
			nil,
			[]string{"A", "B", "HEAD"},
		},
		{
			"migration on the middle of the chain, with two roots",
			map[string]migrationNode{
				"A": {
					NewMigrationFunc("A", nil),
					[]string{},
				},
				"AA": {
					NewMigrationFunc("AA", nil),
					[]string{},
				},
				"B": {
					NewMigrationFunc("B", nil),
					[]string{"A", "AA"},
				},
				"C": {
					NewMigrationFunc("C", nil),
					[]string{"B"},
				},
			},
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{"B"},
			},
			nil,
			nil,
			[]string{"A", "AA", "B", "HEAD"},
		},
		{
			"duplicated names error",
			map[string]migrationNode{
				"A": {
					NewMigrationFunc("A", nil),
					[]string{},
				},
				"AA": {
					NewMigrationFunc("AA", nil),
					[]string{},
				},
				"B": {
					NewMigrationFunc("B", nil),
					[]string{"A", "AA"},
				},
				"C": {
					NewMigrationFunc("C", nil),
					[]string{"B"},
				},
				"HEAD": {
					NewMigrationFunc("HEAD", nil),
					[]string{"B"},
				},
			},
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{"A"},
			},
			MigrationDuplicated,
			nil,
			[]string{},
		},
		{
			"doubled dependencies",
			map[string]migrationNode{
				"A": {
					NewMigrationFunc("A", nil),
					[]string{},
				},
				"B": {
					NewMigrationFunc("B", nil),
					[]string{"A"},
				},
				"C": {
					NewMigrationFunc("C", nil),
					[]string{"B"},
				},
			},
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{"A", "B"},
			},
			nil,
			nil,
			[]string{"A", "B", "HEAD"},
		},
		{
			"dependency not satisfied error",
			map[string]migrationNode{
				"A": {
					NewMigrationFunc("A", nil),
					[]string{},
				},
				"B": {
					NewMigrationFunc("B", nil),
					[]string{"A"},
				},
				"C": {
					NewMigrationFunc("C", nil),
					[]string{"B"},
				},
			},
			args{
				NewMigrationFunc("HEAD", func(migrator *MigratorContext) error {
					return nil
				}),
				[]string{"A", "B", "G"},
			},
			DependencyNotSatisfied,
			nil,
			[]string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, err := gorm.Open(sqlite.Open("/tmp/migrator.db"), &gorm.Config{
				Logger: logger.Default.LogMode(logger.Silent),
			})
			defer func() {
				_ = os.Remove("/tmp/migrator.db")
			}()
			if err != nil {
				t.Errorf("Test initialization error = %v", err)
			}
			migrator := &Migrator{
				db:         db,
				migrations: tt.tree,
			}
			err = migrator.Init()
			if err := migrator.AddMigration(tt.args.migration, tt.args.dependencies); !errors.Is(err, tt.addingErr) {
				t.Errorf("AddMigration() error = %v, epxected %v", err, tt.addingErr)
				return
			}
			if tt.addingErr != nil {
				return
			}
			order := migrator.Order(tt.args.migration.Name())
			if tt.expectedOrder != nil && len(tt.expectedOrder) != len(order) {
				t.Errorf(
					"expected and returned order don't match length: expected=%d returned=%d",
					len(tt.expectedOrder), len(order),
				)
				return
			}
			if tt.expectedOrder != nil {
				for i, item := range order {
					if tt.expectedOrder[i] != item {
						t.Errorf("Order() wrong migrations's order got: %v expected %v", order, tt.expectedOrder)
						return
					}
				}
			}

			if err := migrator.MigrateTo(tt.args.migration.Name()); !errors.Is(err, tt.migratingErr) {
				t.Errorf("MigrateTo() error = %v, epxected %v", err, tt.migratingErr)
				return
			}
			if tt.migratingErr != nil {
				return
			}
		})
	}
}

func TestMigrator_MigrateTo(t *testing.T) {
	type testCase struct {
		output chan string
		migrator *Migrator
		tree map[string]migrationNode
	}
	makeTestCase := func(args map[string][]string, err error) *testCase {
		ch := make(chan string)
		tree := make(map[string]migrationNode)
		for k, v := range args {
			tree[k] = migrationNode{
				Migration: NewMigrationFunc(k, func(k string) func(migrator *MigratorContext) error {
					return func(migrator *MigratorContext) error {
						if err != nil {
							return err
						}
						ch <- k
						return nil
					}
				}(k)),
				Dependencies: v,
			}
		}
		return &testCase{
			output: ch,
			tree: tree,
		}
	}
	tests := []struct {
		name          string
		tcase         *testCase
		premigrateTo string
		migrateTo    string
		sec []string
		err error
	}{
		{
			"single migration",
			makeTestCase(map[string][]string{
				"A": []string{},
			},nil),
			"",
			"A",
			[]string{"A"},
			nil,
		},
		{
			"migration's chain",
			makeTestCase(map[string][]string {
				"A": []string{},
				"B": []string{"A"},
				"C": []string{"A", "B"},
			}, nil),
			"",
			"C",
			[]string{"A", "B", "C"},
			nil,
		},
		{
			"migration's chain with premigrations",
			makeTestCase(map[string][]string {
				"A": []string{},
				"B": []string{"A"},
				"C": []string{"A", "B"},
			}, nil),
			"B",
			"C",
			[]string{"C"},
			nil,
		},
		{
			"migration's chain repeated migration",
			makeTestCase(map[string][]string {
				"A": []string{},
				"B": []string{"A"},
				"C": []string{"A", "B"},
			}, nil),
			"C",
			"C",
			[]string{},
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, err := gorm.Open(sqlite.Open("/tmp/migrator.db"), &gorm.Config{
				Logger: logger.Default.LogMode(logger.Silent),
			})
			defer func() {
				_ = os.Remove("/tmp/migrator.db")
			}()
			if err != nil {
				t.Errorf("Test initialization error = %v", err)
			}
			migrator := &Migrator{
				db:         db,
				migrations: tt.tcase.tree,
			}
			migrator.Init()
			check := func(context context.Context, doCheck bool, notify chan bool) {
				result := func () []string {
					result := make([]string, 0)
					for {
						select {
						case x := <-tt.tcase.output:
							result = append(result, x)
						case <-context.Done():
							return result
						}
					}
				}()
				if doCheck {
					if len(tt.sec) != len(result) {
						t.Errorf("MigrateTo() result given %v, expected %v ", result, tt.sec)

					}
					for i, r := range result {
						if len(tt.sec) <= i || tt.sec[i] != r {
							t.Errorf("MigrateTo() result given %v, expected %v ", result, tt.sec)
							break
						}
					}
				}
				notify <- true
			}
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			wait := make(chan bool)
			go check(ctx, false, wait)
			if tt.premigrateTo != "" {
				if err := migrator.MigrateTo(tt.premigrateTo); !errors.Is(err, tt.err) {
					t.Errorf("MigrateTo() <premigrate> error = %v, expected %v ", err, tt.err)
				}
			}
			cancel()
			<- wait
			ctx, cancel = context.WithTimeout(context.Background(), time.Second)
			wait = make(chan bool)
			go check(ctx, true, wait)
			if err := migrator.MigrateTo(tt.migrateTo); !errors.Is(err, tt.err) {
				t.Errorf("MigrateTo() error = %v, expected %v", err, tt.err)
			}
			cancel()
			<- wait
		})
	}
}

func TestMigrator_MigrateToRepeatLast(t *testing.T) {
	type testCase struct {
		output chan string
		migrator *Migrator
		tree map[string]migrationNode
	}
	makeTestCase := func(args map[string][]string, err error) *testCase {
		ch := make(chan string)
		tree := make(map[string]migrationNode)
		for k, v := range args {
			tree[k] = migrationNode{
				Migration: NewMigrationFunc(k, func(k string) func(migrator *MigratorContext) error {

					return func(migrator *MigratorContext) error {
						if err != nil {
							return err
						}
						ch <- k
						return nil
					}
				}(k)),
				Dependencies: v,
			}
		}
		return &testCase{
			output: ch,
			tree: tree,
		}
	}
	tests := []struct {
		name          string
		tcase         *testCase
		premigrateTo string
		migrateTo    string
		sec []string
		err error
	}{
		{
			"single migration",
			makeTestCase(map[string][]string{
				"A": []string{},
			},nil),
			"",
			"A",
			[]string{"A"},
			nil,
		},
		{
			"migration's chain",
			makeTestCase(map[string][]string {
				"A": []string{},
				"B": []string{"A"},
				"C": []string{"A", "B"},
			}, nil),
			"",
			"C",
			[]string{"A", "B", "C"},
			nil,
		},
		{
			"migration's chain with premigrations",
			makeTestCase(map[string][]string {
				"A": []string{},
				"B": []string{"A"},
				"C": []string{"A", "B"},
			}, nil),
			"B",
			"C",
			[]string{"C"},
			nil,
		},
		{
			"migration's chain repeated migration",
			makeTestCase(map[string][]string {
				"A": []string{},
				"B": []string{"A"},
				"C": []string{"A", "B"},
			}, nil),
			"C",
			"C",
			[]string{"C"},
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, err := gorm.Open(sqlite.Open("/tmp/migrator.db"), &gorm.Config{
				Logger: logger.Default.LogMode(logger.Silent),
			})
			defer func() {
				_ = os.Remove("/tmp/migrator.db")
			}()
			if err != nil {
				t.Errorf("Test initialization error = %v", err)
			}
			migrator := &Migrator{
				db:         db,
				migrations: tt.tcase.tree,
			}
			migrator.Init()
			check := func(context context.Context, doCheck bool, notify chan bool) {
				result := func () []string {
					result := make([]string, 0)
					for {
						select {
						case x := <-tt.tcase.output:
							result = append(result, x)
						case <-context.Done():
							return result
						}
					}
				}()
				if doCheck {
					if len(tt.sec) != len(result) {
						t.Errorf("MigrateTo() result given %v, expected %v ", result, tt.sec)

					}
					for i, r := range result {
						if len(tt.sec) <= i || tt.sec[i] != r {
							t.Errorf("MigrateTo() result given %v, expected %v ", result, tt.sec)
							break
						}
					}
				}
				notify <- true
			}
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			wait := make(chan bool)
			go check(ctx, false, wait)
			if tt.premigrateTo != "" {
				if err := migrator.MigrateTo(tt.premigrateTo); !errors.Is(err, tt.err) {
					t.Errorf("MigrateTo() <premigrate> error = %v, expected %v ", err, tt.err)
				}
			}
			cancel()
			<- wait
			ctx, cancel = context.WithTimeout(context.Background(), time.Second)
			wait = make(chan bool)
			go check(ctx, true, wait)
			if err := migrator.MigrateToRepeatLast(tt.migrateTo); !errors.Is(err, tt.err) {
				t.Errorf("MigrateToRepeatLast() error = %v, expected %v", err, tt.err)
			}
			cancel()
			<- wait
		})
	}
}
