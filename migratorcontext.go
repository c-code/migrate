package migrate

import "gorm.io/gorm"


/*
MigratorContext is provided to migration function as an argument
 */
type MigratorContext struct {
	gorm *gorm.DB
}

func (mc *MigratorContext) GORM() *gorm.DB {
	return mc.gorm
}
