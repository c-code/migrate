package migrate

import (
	"fmt"
	"gorm.io/gorm"
)

/*
MigrationDuplicated is an error returned on an attempt to add migration with a name that has been
already added
 */
var MigrationDuplicated error = fmt.Errorf("migration duplicated")
/*
DependencyNotSatisfied is returned on attempt of adding a migration having dependency that
has not been provided to the migrator
 */
var DependencyNotSatisfied error = fmt.Errorf("dependency not satisfied")
/*
MigrationNotFoundInTheChain is returned when the last saved migration in DB is not present
in the chain of dependency of the migration that Migrator tries tries to migrate to. It's
to prevent data incositency
 */
var MigrationNotFoundInTheChain error = fmt.Errorf("migration cannot be found in a chain")


/*
Migrationer represents a migration.
 */
type Migrationer interface {
	/*
	Name of the migration
	 */
	Name() string
	/*
	Forward is a function that should define a migration. It will be called by a migrator
	when it is the turn of the migration
	 */
	Forward(ctx *MigratorContext) error
}

type migrationNode struct {
	Migration Migrationer
	Dependencies []string
}

/*
Migrator is responsible of performing the migration process. It needs to be provided with
the migrations.
 */
type Migrator struct {
	db *gorm.DB
	migrations map[string]migrationNode
}


/*
NewMigrator creates a Migrator and returns pointer
 */
func NewMigrator(db *gorm.DB) *Migrator {
	return &Migrator{db: db, migrations: make(map[string]migrationNode)}
}

/*
Init creates environment needed for migrator to work correctly. It has an impact in database
by creation own tables.
 */
func (migrator *Migrator) Init() error {
	return migrator.init()
}

func (migrator *Migrator) init() error {
	return migrator.db.AutoMigrate(&migrationVersion{})
}

func (migrator *Migrator) checkDependencies(deps []string) error {
	for _, dep := range deps {
		if _, ok := migrator.migrations[dep]; !ok {
			return fmt.Errorf("%w: %s", DependencyNotSatisfied, dep)
		}
	}
	return nil
}

func (migrator *Migrator) checkDuplications(name string) error {
	if _, ok := migrator.migrations[name]; ok {
		return fmt.Errorf("Migrator.AddMigration() -> %w: %s", MigrationDuplicated, name)
	}
	return nil
}


/*
AddMigration adds migration to the migrator validating the consistency.
migration: is actual migration that provides Name() and Forward() function that will be called as the migration
dependencies: list of names of the migrations that needs to be run before the actual migration
 */
func (migrator *Migrator) AddMigration(migration Migrationer, dependencies []string) error {
	if err := migrator.checkDuplications(migration.Name()); err != nil {
		return fmt.Errorf("Migrator.AddMigration() -> %w", err)
	}
	if err := migrator.checkDependencies(dependencies); err != nil {
		return fmt.Errorf("Migrator.AddMigration() -> %w", err)
	}
	migrator.migrations[migration.Name()] = migrationNode{
		Migration: migration,
		Dependencies: dependencies,
	}
	return nil
}

func (migrator *Migrator) order(node migrationNode) []migrationNode {
	result := make([]migrationNode, 0)
	added := make(map[string]bool)
	for _, dep := range node.Dependencies {
		for _, r := range migrator.order(migrator.migrations[dep]) {
			if _, ok := added[r.Migration.Name()]; !ok {
				result = append(result, r)
				added[r.Migration.Name()] = true
			}
		}
	}
	result = append(result, node)
	return result
}

/*
Order returns the order in which the migrations would be run if you needed migrating to the migration given by name
 */
func (migrator *Migrator) Order(name string) []string {
	if node, ok := migrator.migrations[name]; ok {
		ordered := migrator.order(node)
		result := make([]string, len(ordered))
		for i, item := range ordered {
			result[i] = item.Migration.Name()
		}
		return result
	}
	return nil
}


func (migrator *Migrator) findStartingMigration() (string, error) {
	result := migrationVersion{}
	res := migrator.db.Last(&result)
	if res.Error == gorm.ErrRecordNotFound {
		return "", nil
	}
	if res.Error != nil {
		return "", res.Error
	}
	return result.Name, nil
}
func (migrator *Migrator) updateMigrationInDB(name string) error {
	migrator.db.Create(&migrationVersion{
		Name: name,
	})
	return nil
}

func (migrator *Migrator) indexOfMigration(name string, list []migrationNode) (int, error) {
	for i, node := range list {
		if node.Migration.Name() == name {
			return i, nil
		}
	}
	return -1, MigrationNotFoundInTheChain
}

func (migrator *Migrator) migrateTo(migrationName string, verbose bool, repeatLast bool) error {
	order := migrator.order(migrator.migrations[migrationName])
	start, err := migrator.findStartingMigration()
	if err != nil {
		return err
	}
	var index int
	if start == "" {
		index = 0
	} else {
		index, err = migrator.indexOfMigration(start, order)
		if !(repeatLast && index == len(order) - 1) {
			index += 1
		}
		if err != nil {
			return err
		}
	}

	for i, migration := range order[index:] {
		if verbose {
			fmt.Printf("%d: %s\n", i, migration.Migration.Name())
		}
		err := migration.Migration.Forward(&MigratorContext{gorm: migrator.db})
		if err != nil {
			if verbose {
				fmt.Println(fmt.Errorf("error: %w", err))
			}
			return err
		}
		err = migrator.updateMigrationInDB(migration.Migration.Name())
		if err != nil {
			if verbose {
				fmt.Println(fmt.Errorf("error: %w", err))
			}
			return err
		}
	}
	return nil
}

/*
MigrateTo performs actual migration process migrating to the migration given by name.
If the last performed migration is the given one, no migration will be applied.
 */
func (migrator *Migrator) MigrateTo(migrationName string) error {
	return migrator.migrateTo(migrationName, false, false)
}

/*
MigrateToRepeatLast does what MigrateTo does with the difference that if the last performed migration is the
given one it repeats the migration.
 */
func (migrator *Migrator) MigrateToRepeatLast(migrationName string) error {
	return migrator.migrateTo(migrationName, false, true)
}

/*
MigrateToRepeatLastWithVerbose  does what MigrateToRepeatLast does but it says currently run migration.
 */
func (migrator *Migrator) MigrateToRepeatLastWithVerbose(migrationName string) error {
	return migrator.migrateTo(migrationName, true, true)
}

/*
MigrateToWithVerbose does exactly what MigrateTo does but it also says the migration it is currently run.
 */
func (migrator *Migrator) MigrateToWithVerbose(migrationName string) error {
	return migrator.migrateTo(migrationName, true, false)
}