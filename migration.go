package migrate


/*
MigrationFunc takes function to migrate forward. The provided function is executed when it's the turn of the migration
to migrate. E.g:

migrator := NewMigrator(db)
migrator.AddMigration("create database", func(ctx *MigratorContext) {
	ctx.GORM().AutoMigrate()
})

 */
type MigrationFunc struct {
	name string
	forward func(ctx *MigratorContext) error
}

/*
Forward applies a migration by calling the provided function.
 */
func (m *MigrationFunc) Forward(migratorCtx *MigratorContext) error {
	if m.forward != nil {
		return m.forward(migratorCtx)
	}
	return nil
}

/*
Name returns the name of the migration
 */
func (m *MigrationFunc) Name() string {
	return m.name
}

/*
NewMigrationFunc creates a migration and returns the pointer.
name is the name of the migrations
forward is the function that will be called as the migration.
*/
func NewMigrationFunc(name string, forward func(migrator *MigratorContext) error) *MigrationFunc {
	return &MigrationFunc{
		name: name,
		forward: forward,
	}
}
