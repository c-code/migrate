# Migrate

### Goal
the goal of the project is to provide library extending GORM by managing a migration history.
The migrations' history, in opposite to common trends, is progressive. It means you can't undo the
migrations. The reason is simple. Most of the cases when you need backward migrations focuses around a deployment of 
a new version, and even then you want to revert only that you just applied. Backward migrations on a long-running
application a rare and even so should be rather done in a progressive way as the new migration.

### Usage
```go
//assuming we have:
//var db gorm.DB
//type Product struct {}
migrator := NewMigrator(&db)
migrator.AddMigration(NewMigrationFunc("create database", func(m *Migrator) {
	migrator.DB.AutoMigrate(&Product)
	return nil
}))

migrator.Init()
migrator.MigrateTo("create database")

```