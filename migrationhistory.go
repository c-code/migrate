package migrate

import "gorm.io/gorm"


type migrationVersion struct {
	gorm.Model
	Name string
}

